from flask import Flask, jsonify

app = Flask(__name__)

vehicules = [
    {
        'id': 627,
        'marque': 'Toyota',
        'modele': 'Corolla',
        'annee': 2015,
        'kilometrage': 50000,
        'prix': 15000.00,
        'carburant': 'Essence',
        'boite_de_vitesse': 'Manuelle',
        'couleur': 'Blanc',
        'description': 'Une voiture fiable et économique.',
        'image_url': 'https://ste-foytoyota.com/wp-content/uploads/2019/08/toyota-corolla-hybrid-2020-4.jpg',
        'porte': 4,
        'place': 5,
        'certificat_critair': 2,
        'est_vendable': True,
        'sold': False
    },
    {
        'id': 628,
        'marque': 'Honda',
        'modele': 'Civic',
        'annee': 2016,
        'kilometrage': 60000,
        'prix': 16000.00,
        'carburant': 'Diesel',
        'boite_de_vitesse': 'Automatique',
        'couleur': 'Noir',
        'description': 'Une voiture sportive et élégante.',
        'image_url': 'https://1.bp.blogspot.com/-M56157Fue6I/X3RsSoriFbI/AAAAAAAAcLs/-xBwisaVmCYoUf92CYRFr_Qzj6aNydrOwCLcBGAsYHQ/s1286/honda-civic-sedan-noir-cristal-crystal-black-pearl.jpg',
        'porte': 4,
        'place': 5,
        'certificat_critair': 2,
        'est_vendable': True,
        'sold': False
    },
    {
        'id': 629,
        'marque': 'Ford',
        'modele': 'Focus',
        'annee': 2017,
        'kilometrage': 40000,
        'prix': 14000.00,
        'carburant': 'Essence',
        'boite_de_vitesse': 'Manuelle',
        'couleur': 'Bleu',
        'description': 'Une compacte polyvalente et performante.',
        'image_url': 'https://img.paruvendu.fr/media_ext/_https_/media.paruvendu.fr/78/37/L21lZGlhLXBhLzk5MjYvNS8wLzk5MjY1MDgwMzdfMS5qcGc_MjAyNDA1MTcxNTM3Mzg_rct?func=crop&w=1000&gravity=auto',
        'porte': 5,
        'place': 5,
        'certificat_critair': 1,
        'est_vendable': True,
        'sold': False
    },
    {
        'id': 630,
        'marque': 'BMW',
        'modele': '320i',
        'annee': 2018,
        'kilometrage': 30000,
        'prix': 25000.00,
        'carburant': 'Essence',
        'boite_de_vitesse': 'Automatique',
        'couleur': 'Gris',
        'description': 'Une berline de luxe avec des performances exceptionnelles.',
        'image_url': 'https://www.bpsnext.com/data/cars/4916/resized_1496308175c71296dd4878.jpg',
        'porte': 4,
        'place': 5,
        'certificat_critair': 2,
        'est_vendable': True,
        'sold': False
    },
    {
        'id': 631,
        'marque': 'Mercedes',
        'modele': 'C200',
        'annee': 2019,
        'kilometrage': 20000,
        'prix': 30000.00,
        'carburant': 'Diesel',
        'boite_de_vitesse': 'Automatique',
        'couleur': 'Rouge',
        'description': 'Une voiture de classe avec un confort suprême.',
        'image_url': 'https://images.netdirector.co.uk/gforces-auto/image/upload/w_1540,h_513,q_auto,c_fill,f_auto,fl_lossy/auto-client/4bc54a0eff3a394b12226366f972e04e/amg_line.jpg',
        'porte': 4,
        'place': 5,
        'certificat_critair': 1,
        'est_vendable': True,
        'sold': False
    }
]
@app.route('/vehicules', methods=['GET'])
def get_vehicles():
    unsold_vehicules = [vehicule for vehicule in vehicules if not vehicule['sold']]
    return jsonify(unsold_vehicules)

@app.route('/vehicule/<int:vehicule_id>', methods=['GET'])
def get_vehicle(vehicule_id):
    vehicule = next((vehicule for vehicule in vehicules if vehicule['id'] == vehicule_id), None)
    if vehicule:
        return jsonify(vehicule), 200
    else:
        return jsonify({'message': 'Voiture non trouvée'}), 404

@app.route('/vehicule/<int:vehicule_id>/sell', methods=['POST'])
def sell_vehicle(vehicule_id):
    vehicule = next((vehicule for vehicule in vehicules if vehicule['id'] == vehicule_id), None)
    if vehicule:
        vehicule['sold'] = True
        return jsonify({'message': 'Voiture vendu !', 'Voiture': vehicule}), 200
    else:
        return jsonify({'message': 'Voiture non trouvé'}), 404

if __name__ == '__main__':
    app.run(debug=True, port=5001)
