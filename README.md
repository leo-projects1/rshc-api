# API de Gestion des Véhicules

Cette API Flask permet de gérer une liste de véhicules d'occasion. Elle fait partie intégrante du projet `RSHC`, un site de revente de véhicules d'occasion. Vous pouvez trouver le projet principal ici : [RSHC sur GitLab](https://gitlab.com/leo-projects1/rshc).

Cette API représente les données d'un site fictif de véhicules en ligne.

## Prérequis

Assurez-vous d'avoir Python et `pip` installés sur votre système. Vous aurez également besoin de `virtualenv` pour créer un environnement virtuel.

## Installation

1. Clonez le dépôt et accédez au répertoire du projet.

2. Créez un environnement virtuel :

   ```bash
   python -m venv venv
   ```

3. Activez l'environnement virtuel :

   - Sur Windows :
     ```bash
     venv\Scripts\activate
     ```
   - Sur macOS/Linux :
     ```bash
     source venv/bin/activate
     ```

4. Installez les dépendances à partir du fichier `requirements.txt` :

   ```bash
   pip install -r requirements.txt
   ```

## Exécution de l'API

Pour démarrer l'API Flask, exécutez le script `app.py` :

```bash
python app.py
```

L'API sera disponible à l'adresse http://127.0.0.1:5001, Changez le port si nécessaire :

```python
if __name__ == '__main__':
    app.run(debug=True, port=5001)
```

## Endpoints

### Récupérer tous les véhicules

- **URL** : `/vehicules`
- **Méthode** : `GET`

### Récupérer un véhicule par ID

- **URL** : `/vehicule/<int:vehicule_id>`
- **Méthode** : `GET`

### Vendre un véhicule

- **URL** : `/vehicule/<int:vehicule_id>/sell`
- **Méthode** : `POST`
